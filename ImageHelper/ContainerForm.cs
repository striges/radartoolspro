﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using BitmapData = System.Drawing.Imaging.BitmapData;

namespace ImageHelper
{
    public partial class ContainerForm : Form
    {
        public ContainerForm()
        {
            InitializeComponent();
        }

        private void ContainerForm_Load(object sender, EventArgs e)
        {
        }


        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            var image = pictureBox1.Image;
            var a = new Bitmap(image);
            for (int i = 0; i < image.Width; i++)
            {
                var p = a.GetPixel(i, 100);
            }
        }

        public static int[] CountModeInPicture(Bitmap p1)
        {
            var b = p1;
            //0 - red, 1 - blue, 2 - green, 3 - black
            var count = new int[] {0, 0, 0, 0};
            for (int i = 0; i < b.Width; i++)
            {
                var p = b.GetPixel(i, 100);
                if (p.R > 250 && p.G < 5 && p.B < 5) //Red
                    count[0]++;
                if (p.R < 5 && p.G > 250 && p.B < 5) //Green
                    count[1]++;
                if (p.R < 5 && p.G < 5 && p.B > 250) //Blue
                    count[2]++;
                if (p.R < 5 && p.G < 5 && p.B < 5)   //Black
                    count[3]++;
            }
            return count;
        }
    }
}
