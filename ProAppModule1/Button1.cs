﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArcGIS.Desktop.Framework;
using ArcGIS.Desktop.Framework.Contracts;
using System.Windows;
using ArcGIS.Desktop.Mapping;
using ArcGIS.Desktop.Framework.Threading.Tasks;
using System.IO;
using ArcGIS.Core.CIM;
using System.Diagnostics;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;

namespace ProAppModule1
{
    internal class Button1 : Button
    {

        public Button1()
        { }

        public Layer AddShapeFileToDocument(string shpPath, string layerName)
        {
            var activeView = MapView.Active?.Map;
            //parameter check
            if (activeView == null)
            {
                return null;
            }

            if(!File.Exists(shpPath))
            {
                return null;
            }

            var shpUri = new Uri(shpPath);

            return LayerFactory.CreateLayer(shpUri, activeView, layerName: layerName);
        }

        protected override async void OnClick()
        {
            await QueuedTask.Run(() =>
            {
                //If not loaded
                var wsrShp = DownloadRadarLayer();
                //Load feature
                var wsrLayer = AddShapeFileToDocument(wsrShp, "WSR-88D Stations");
                //Set label
                var labelLayer = wsrLayer as FeatureLayer;

                if (labelLayer != null)
                {

                    var l = labelLayer.GetDefinition() as CIMBasicFeatureLayer;
                    var t = l.FeatureTable;
                    t.DisplayField = "SITE";
                    labelLayer.SetLabelVisibility(true);
                }
            });
        }

        protected string DownloadRadarLayer()
        {
            //The radar layer will put in %APPDATA%/RadarTools/maps
            var radarToolsDir = GlobalSettings.RadarToolsDir;
            var wctMapPath = Path.Combine(radarToolsDir, "wctmap.zip");
            var wsrShp = Path.Combine(radarToolsDir, "shapefiles", "wsr.shp");

            //Download wct if not exists
            if (!File.Exists(wctMapPath))
            {
                //These lines should be useless
                Directory.CreateDirectory(radarToolsDir);
                Directory.CreateDirectory(Path.Combine(radarToolsDir, "shapefiles"));
                DownloadWctMap(wctMapPath);
                ExtractWctMap(wctMapPath);
            }
            return wsrShp;
        }

        private void DownloadWctMap(string wctMapPath)
        {

            // The radar layer will put in %APPDATA%/RadarTools/maps
            var client = new WebClient();
            try
            {
                client.DownloadFile("http://www1.ncdc.noaa.gov/pub/data/wct/dist/wct-MapData-1.5.jar", wctMapPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void ExtractWctMap(string wctPath)
        {
            //Change pwd to where we extract map
            var pwd = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Path.GetFullPath(wctPath)));
            //Extract files
            using (var s = new ZipInputStream(File.OpenRead(wctPath)))
            {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {
                    Console.WriteLine(theEntry.Name);
                    var directoryName = Path.GetDirectoryName(theEntry.Name);
                    var fileName = Path.GetFileName(theEntry.Name);
                    if (!string.IsNullOrEmpty(directoryName))
                    {
                        Directory.CreateDirectory(directoryName);
                    }
                    if (fileName != string.Empty)
                    {
                        using (var streamWriter = File.Create(theEntry.Name))
                        {
                            int size;
                            var data = new byte[2048];
                            while (true)
                            {
                                size = s.Read(data, 0, data.Length);
                                if (size > 0)
                                {
                                    streamWriter.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            //Set pwd back;
            Directory.SetCurrentDirectory(pwd);
        }
    }

}
