﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArcGIS.Desktop.Framework;
using ArcGIS.Desktop.Framework.Contracts;
using System.Windows.Forms;
using System.Diagnostics;
using ArcGIS.Desktop.Framework.Threading.Tasks;
using ArcGIS.Desktop.Core.Geoprocessing;
using System.Threading;

namespace ProAppModule1
{
    public class ButtonAddRef : ArcGIS.Desktop.Framework.Contracts.Button
    {
        public string ReflectivityImagePath { get; set; }

        protected override void OnClick()
        {
            try
            {
                var selectDateForm = new SelectDateDialog(this);
                var result = selectDateForm.ShowDialog();
                if (DialogResult.OK == result)
                {
                    AddImage(ReflectivityImagePath);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }
         
        private async void AddImage(string imagePath)
        {
            string tool_path = "111";
            string[] args = { };
            var _cts = new CancellationTokenSource();
            var result1 = await Geoprocessing.ExecuteToolAsync(tool_path, args, null, _cts.Token);
        }
    }
}
