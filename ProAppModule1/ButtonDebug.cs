﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArcGIS.Desktop.Framework;
using ArcGIS.Desktop.Framework.Contracts;
using ArcGIS.Desktop.Framework.Dialogs;

namespace ProAppModule1
{
    internal class ButtonDebug : Button
    {
        protected override void OnClick()
        {
#if DEBUG
            MessageBox.Show(GlobalSettings.ExecuteImagePath);
#endif

        }
    }
}
