﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProAppModule1
{
    public static class GlobalSettings
    {
        public static string RadarToolsDir
        {
            get { return Environment.ExpandEnvironmentVariables(@"%APPDATA%\RadarTools\maps"); }
        }

        public static string ExecuteImagePath
        {
            get { return Assembly.GetEntryAssembly().Location; }
        }

        static GlobalSettings()
        {

        }
    }
}
