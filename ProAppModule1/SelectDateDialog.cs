﻿using System;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace ProAppModule1
{
    public partial class SelectDateDialog : Form
    {
        public static DateTime LastSelection = new DateTime(2004, 8, 13);
        private readonly ButtonAddRef caller;
        private DateTime previousDate;

        public SelectDateDialog(ButtonAddRef caller)
        {
            InitializeComponent();
            this.caller = caller;
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "yyyy-MM-dd HH:mm";
            dateTimePicker1.MinDate = new DateTime(1995, 1, 1);
            dateTimePicker1.MaxDate = DateTime.Now;
            dateTimePicker1.Value = LastSelection;
            previousDate = dateTimePicker1.Value;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker1.Value.Minute%5 != 0)
            {
                if (dateTimePicker1.Value < previousDate ||
                    (previousDate.Minute == 0 && dateTimePicker1.Value.Minute == 59))
                {
                    dateTimePicker1.Value = dateTimePicker1.Value.AddMinutes(-4);
                }
                else
                {
                    dateTimePicker1.Value = dateTimePicker1.Value.AddMinutes(4);
                    if (dateTimePicker1.Value.Minute == 0)
                    {
                        dateTimePicker1.Value = dateTimePicker1.Value.AddHours(-1);
                    }
                }
            }
            previousDate = dateTimePicker1.Value;
        }

        private void SelectDate_Load(object sender, EventArgs e)
        {
        }

        private string DownloadImage(DateTime d)
        {
            //The radar layer will put in %APPDATA%/RadarTools/maps
            var appDataDir = Environment.GetEnvironmentVariable("APPDATA");
            var radarToolsDir = appDataDir + "\\RadarTools";
            if (!Directory.Exists(radarToolsDir)) Directory.CreateDirectory(radarToolsDir);
            var imgDir = radarToolsDir + "\\Images";
            if (!Directory.Exists(imgDir)) Directory.CreateDirectory(imgDir);
            var baseurl = "http://mesonet.agron.iastate.edu/archive/data/" + d.Year + "/" +
                          d.Month.ToString().PadLeft(2, '0') + "/" + d.Day.ToString().PadLeft(2, '0')
                          + "/GIS/uscomp/" + "n0r_" + d.ToString("yyyyMMddHHmm");
            var w = new WebClient();
            var png = imgDir + "\\n0r_" + d.ToString("yyyyMMddHHmm") + ".png";
            var wld = imgDir + "\\n0r_" + d.ToString("yyyyMMddHHmm") + ".wld";
            if (!File.Exists(png))
            {
                w.DownloadFile(baseurl + ".png", png);
                w.DownloadFile(baseurl + ".wld", wld);
            }
            return png;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            caller.ReflectivityImagePath = DownloadImage(dateTimePicker1.Value);
            DialogResult = DialogResult.OK;
            LastSelection = dateTimePicker1.Value;
            Close();
        }
    }
}